package app.peacenepal.onepass

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import app.peacenepal.onepass.fingerPrint.Fingerprint

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Fingerprint.getInstance().setContext(this)
        Fingerprint.getInstance().setHandler(fingerprintHandler)
        Fingerprint.getInstance().Open()
        Fingerprint.getInstance().Process()
    }

    @SuppressLint("HandlerLeak")
    private val fingerprintHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                Fingerprint.STATE_PLACE -> {
                    Log.d("MainActivity", "STATE_PLACE")
                }
                Fingerprint.STATE_LIFT ->{
                    Log.d("MainActivity", "STATE_LIFT")
                }
                Fingerprint.STATE_GETIMAGE -> {
                    Log.d("MainActivity", "STATE_GETIMAGE")
                }
                Fingerprint.STATE_UPIMAGE -> {
                    Log.d("MainActivity", "STATE_UPIMAGE")
                }
                Fingerprint.STATE_GENDATA -> {
                    Log.d("MainActivity", "STATE_GENDATA")
                }
                Fingerprint.STATE_UPDATA -> {
                    Log.d("MainActivity", "STATE_UPDATA")
                }
                Fingerprint.STATE_FAIL -> {
                    Log.d("MainActivity", "STATE_Failed")
                    Fingerprint.getInstance().Process()
                }
            }
        }
    }
}
