package app.peacenepal.onepass.fingerPrint;

import android.content.Context;
import android.os.Handler;
import androidSerialPortApi.AsyncFingerprint;
import androidSerialPortApi.SerialPortManager;

public class Fingerprint {
	
	public static final int STATE_NONE = 0;
	public static final int STATE_PLACE = 1;	
	public static final int STATE_LIFT = 2;
	public static final int STATE_GETIMAGE = 3;
	public static final int STATE_UPIMAGE = 4;
	public static final int STATE_GENDATA = 5;
	public static final int STATE_UPDATA = 6;
	public static final int STATE_FAIL = 7;
	
	private static Fingerprint instance;	
	
	public static Fingerprint getInstance() {
    	if(null == instance) {
    		instance = new Fingerprint();
    	}
    	return instance;
    }
	
	private Context pcontext=null;
	private Handler phandler=null;
	
	private AsyncFingerprint vFingerprint;
    private boolean			 bfpWork=false;
    private boolean			 bIsUpImage=true;
    private boolean			 bIsCancel=false;
    	
	
	public void setContext(Context context){
		pcontext=context;
    }
	
	public void setHandler(Handler handler){
		phandler=handler;
    }
	
	private void SendMessage(int msg,byte[] data){
		if(phandler!=null)
			phandler.obtainMessage(msg, data).sendToTarget();
	}
	
	public void SetUpImage(boolean isup){
		bIsUpImage=isup;
	}
	
	public boolean IsUpImage(){
		return bIsUpImage;
	}
	
	public void Open(){
		vFingerprint = SerialPortManager.getInstance().getNewAsyncFingerprint();
		
		vFingerprint.setOnGetImageExListener(new AsyncFingerprint.OnGetImageExListener() {
            @Override
            public void onGetImageExSuccess() {
            	SendMessage(STATE_GETIMAGE,null);
                if(bIsUpImage){
                    vFingerprint.FP_UpImageEx();                    
                }else{
                    vFingerprint.FP_GenCharEx(1);
                }
            }

            @Override
            public void onGetImageExFail() {
                if(!bIsCancel){
                    vFingerprint.FP_GetImageEx();
                }else{
                	bIsCancel=false;
                	bfpWork=false;
                }
            }
        });

        vFingerprint.setOnUpImageExListener(new AsyncFingerprint.OnUpImageExListener() {
            @Override
            public void onUpImageExSuccess(byte[] data) {
                SendMessage(STATE_UPIMAGE,data);
                vFingerprint.FP_GenCharEx(1);
            }

            @Override
            public void onUpImageExFail() {
                bfpWork=false;
                SendMessage(STATE_FAIL,null);
            }
        });

        vFingerprint.setOnGenCharExListener(new AsyncFingerprint.OnGenCharExListener() {
            @Override
            public void onGenCharExSuccess(int bufferId) {
            	SendMessage(STATE_GENDATA,null);
                vFingerprint.FP_UpChar();
            }

            @Override
            public void onGenCharExFail() {
            	bfpWork=false;
                SendMessage(STATE_FAIL,null);
            }
        });

        vFingerprint.setOnUpCharListener(new AsyncFingerprint.OnUpCharListener() {

            @Override
            public void onUpCharSuccess(byte[] model) {
            	bfpWork=false;
            	SendMessage(STATE_UPDATA,model);
            }

            @Override
            public void onUpCharFail() {
            	bfpWork=false;
                SendMessage(STATE_FAIL,null);
            }
        });
	}
	
	public void Close(){
		bIsCancel=true;
        bfpWork=false;
        SerialPortManager.getInstance().closeSerialPort();
	}
	
	public void Process(){		
		if(!bfpWork){
			bIsCancel=false;
			SendMessage(STATE_PLACE,null);
            vFingerprint.FP_GetImageEx();
            bfpWork=true;
        }
	}
	
	public void Cancel(){
		bIsCancel=true;
	}
	
}
